<?php
/**
* @package    amigatlk
* @subpackage amigatlk
* @author     Bruno Ethvignot <bruno at tlk dot biz> 
* @copyright  TLK Games
* @created    2013-04-01
* @date       2020-10-18 
* @link       http://amiga.tlk.fr/
* @license    All rights reserved
*/
jClasses::inc('content');
class games extends content {

    private function getGamesPath() {
        return jApp::appPath( 'games');
    }

    public function getProduct($name) {
        $game = $this->getContent($name);
        if ( empty ($game) ) {
            jLog::log( 'The game "' . $name . '" was not found!' ); 
            return $game;
        }
        $game->code  = $name; 
        $screenshots = trim ( array_shift($game->content) );
        if ( empty ($screenshots) ) {
            $game->screenshots = array();
        } else {
            $game->screenshots = explode( ",", $screenshots );
        }
        return $game;
    }
}
?>
