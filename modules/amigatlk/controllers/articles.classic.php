<?php
/**
* @package    amigatlk
* @subpackage amigatlk
* @author     Bruno Ethvignot <bruno at tlk dot biz> 
* @copyright  TLK Games
* @created    2013-06-09
* @date       2020-10-10 
* @link       http://amiga.tlk.fr/
* @license    GNU GPL v3
*/

class articlesCtrl extends jController {
    
    /**
     *
     */
    function view() {
        $name = $this->param('name');
        $rep = $this->getResponse('html');
        $articles = jClasses::getService('amigatlk~articles');
        $article = $articles->getArticle( $this->param('name') );
        if (empty ($article)) {
            $rep->body->assignZone( 'MAIN', 'amigatlk~notFound404' );
            $rep->setHttpStatus( '404', 'Not Found' );
            return $rep;
        }
        $rep->title = $article->title;
        $rep->body->assignZone('MAIN',
            'amigatlk~viewArticle',
            array('article' => $article));
        return $rep;
    }
}
