<?php
/**
* @package   amigatlk
* @subpackage amigatlk
* @author    Bruno Ethvignot
* @copyright 2020 TLK Games
* @link      http://www.tlkgames.com/
* @license   https://www.gnu.org/licenses/gpl-3.0.txt All rights reserved
*/

class defaultCtrl extends jController {
    /**
    *
    */
    function index() {
        $lang = jLocale::getCurrentLang();
        if ( $lang != 'fr' ) {
            $rep = $this->getResponse('redirect');
            $rep->action = "amigatlk~default:index";
            $rep->params = array('lang' => 'fr');
            return $rep;
        }
        $rep = $this->getResponse('html');
        $articles = jClasses::getService('amigatlk~articles');
        $article  = $articles->getArticle( 'homepage' );
        if ( empty ($article) ) {
            $rep->body->assignZone( 'MAIN', 'amigatlk~notFound404' );
            $rep->setHttpStatus( '404', 'Not Found' );
            return $rep;
        }
        $rep->body->assignZone( 'MAIN', 'amigatlk~viewArticle', 
            array( 'article' => $article ) );
        return $rep;
    }
}
