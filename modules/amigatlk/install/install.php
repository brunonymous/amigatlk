<?php
/**
* @package   amigatlk
* @subpackage amigatlk
* @author    Bruno Ethvignot
* @copyright 2020 TLK Games
* @link      http://www.tlkgames.com/
* @license   https://www.gnu.org/licenses/gpl-3.0.txt All rights reserved
*/


class amigatlkModuleInstaller extends \Jelix\Installer\Module\Installer {

    function install(\Jelix\Installer\Module\API\InstallHelpers $helpers) {
        //$helpers->database()->execSQLScript('sql/install');

        /*
        jAcl2DbManager::addRole('my.role', 'amigatlk~acl.my.role', 'role.group.id');
        jAcl2DbManager::addRight('admins', 'my.role'); // for admin group
        */
    }
}