<?php
/**
* @package   amigatlk
* @subpackage amigatlk
* @author    Bruno Ethvignot
* @copyright 2020 TLK Games
* @link      http://www.tlkgames.com/
* @license   https://www.gnu.org/licenses/gpl-3.0.txt All rights reserved
*/


class amigatlkModuleConfigurator extends \Jelix\Installer\Module\Configurator {

    public function getDefaultParameters() {
        return array();
    }

    function configure(\Jelix\Installer\Module\API\ConfigurationHelpers $helpers) {

    }
}