<?php
/**
* @package   amigatlk
* @author    Bruno Ethvignot
* @copyright 2020 TLK Games
* @link      http://www.tlkgames.com/
* @license   https://www.gnu.org/licenses/gpl-3.0.txt All rights reserved
*/

require_once (__DIR__.'/../application.init.php');

\Jelix\Scripts\Configure::launch();
