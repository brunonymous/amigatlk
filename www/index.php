<?php
/**
* @package   amigatlk
* @subpackage 
* @author    Bruno Ethvignot
* @copyright 2020 TLK Games
* @link      http://www.tlkgames.com/
* @license   https://www.gnu.org/licenses/gpl-3.0.txt All rights reserved
*/

require ('../application.init.php');
require (JELIX_LIB_CORE_PATH.'request/jClassicRequest.class.php');

checkAppOpened();

jApp::loadConfig('index/config.ini.php');

jApp::setCoord(new jCoordinator());
jApp::coord()->process(new jClassicRequest());



